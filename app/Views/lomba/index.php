<?php $this->extend('layout/template'); ?>
<?php $this->section('konten'); ?>
<?php #dd($pstPantomim); ?>
<div class="container">
    <div class="row">
        <div class="col">
        <div class="jumbotron jumbotron-fluid" style="background-image: url('https://m.ayojakarta.com/images-jakarta/post/articles/2021/06/18/35295/barley-field-1684052_640.jpg'); background-size:cover;">
  <div class="container">
    <h1 class="display-4">Lomba Eksta Kurikuler</h1>
    <p class="lead">Aneka Lomba Memeriahkan Hari Jadi SLB Negeri Banjarnegara</p>
  <div>

</div>
        </div>
    </div>
    <div class="row">
        <div class="col">
        <?php if (session()->getFlashdata('pesan')) : ?>
            <div class="alert alert-warning alert-dismissible fade show" role="alert">
  <strong><?= session()->getFlashdata('pesan'); ?></strong>
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
        <?php endif; ?>
        </div>
    </div>
    <div class="row">
    <!-- informasi mata lomba -->
    <?php 
    $matalomba =[
        [ 'nama' =>'Lomba Melukis','info' => 'Tema: Desa Tambakan','gambar' => '/img/pelukis.png','link' => '/lomba/ikut/Lukis'],
        ['nama' =>'Lomba Pantomim','info' => 'Tema: Panen Mangga','gambar' => '/img/pantomim.png','link' => '/lomba/ikut/Pantomim'],
        ['nama' =>'Bulu Tangkis','info' => 'Buluangkis Tunggal','gambar' => '/img/bulutangkis.png','link' => '/lomba/ikut/Badminton'],
        ['nama' =>'Coding Website','info' => 'Membuat Website kelas','gambar' => '/img/coding.png','link' => '/lomba/ikut/Programming']
    ];
    ?>
    <?php foreach($matalomba as $ml ): ?> 
        <div class="card col-lg-3 border-0 px-2" style="width: 18rem;">
        <img src="<?= $ml['gambar']; ?>" class="card-img-top" alt="<?= $ml['nama']; ?>">
        <div class="card-body">
            <h5 class="card-title"><?= $ml['nama']; ?></h5>
            <p class="card-text"><?= $ml['info']; ?></p>
            <a href="<?= $ml['link']; ?>" class="btn btn-primary">Ikut</a>
        </div>
        </div>
     <?php endforeach; ?>
        <!-- <div class="col"></div> -->
    </div>
    <div class="row">
        <!-- Peserta Lomba Lukis -->
        <div class="col-lg-3">
            <div class="list-group">
                <li class="list-group-item bg-dark text-light text-center">Peserta Lomba Lukis</li>
                <?php foreach( $pstLukis as $pst ): ?>
                <li class="list-group-item py-l"><a href="/lomba/detail/Lukis/<?= $pst['noUrut']; ?>" class="mr-2">info</a> <?= $pst['namaPeserta']; ?> </li>
                 <?php endforeach; ?>
            </div>
        </div>
        
        <!-- Peserta Lomba pantomim -->
        <div class="col-lg-3">
            <div class="list-group">
                <li class="list-group-item bg-dark text-light text-center">Peserta Lomba Pantomim</li>
                <?php foreach( $pstPantomim as $pst ): ?>
                <li class="list-group-item py-l"><a href="/lomba/detail/Pantomim/<?= $pst['noUrut']; ?>" class="mr-2">info</a> <?= $pst[ 'namaPeserta' ]; ?> </li>
                 <?php endforeach; ?>
            </div>
        </div>
        
        <!-- Peserta Lomba bulu tangkis -->
        <div class="col-lg-3">
            <div class="list-group">
                <li class="list-group-item bg-dark text-light text-center">Peserta Lomba Bulu Tangkis</li>
                <?php foreach( $pstBulutangkis as $pst ): ?>
                <li class="list-group-item py-l"><a href="/lomba/detail/Badminton/<?= $pst['noUrut']; ?>" class="mr-2">info</a> <?= $pst[ 'namaPeserta' ]; ?> </li>
                 <?php endforeach; ?>
            </div>
        </div>
        
        <!-- Peserta Lomba Programming -->
        <div class="col-lg-3">
            <div class="list-group">
                <li class="list-group-item bg-dark text-light text-center">Peserta Lomba Programming</li>
                <?php foreach( $pstProgramming as $pst ): ?>
                <li class="list-group-item py-l"><a href="/lomba/detail/Programming/<?= $pst['noUrut']; ?>" class="mr-2">info</a> <?= $pst[ 'namaPeserta' ]; ?> </li>
                 <?php endforeach; ?>
            </div>
        </div>
       
    </div>
    <div class="rosw">
        <div class="col my-3 text-center"><small>Copyright @2021 SLB Negeri Banjarnegara</small></div>
    </div>
</div>
<?php $this->endSection(); ?>