<?php

namespace App\Models;

use CodeIgniter\Model;

class PesertalombaModel extends Model
{
    protected $table      = 'pesertalomba';
    protected $primaryKey = 'noUrut';
    protected $useSoftDeletes = true;
    protected $allowedFields = ['jnLomba', 'noPendaftaran','namaPeserta','jnKelamin'];
    protected $useTimestamps = true;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    // daftar peserta berasarkan jenis lomba
    public function pelomba($lomba,$noUrut=false){
        if($noUrut==false){
            return $this->where('jnLomba',$lomba)->orderBy('namaPeserta')->findAll(0,20);
        }else{
            return $this->find($noUrut);
        }
    }

    // fungsi untuk menmbah dan mengubah data peserta lomba
    public function tambahpeserta($data){
        return $this->save ($data);
    }
    // fungsi menhapus peserta lomba
    public function mundur($data)
    {
        $this->where($data)->delete();
        return $this->db->affectedRows();
    }

    // fungsi mendapatka nomor pendaftaran baru
    public function nomorBaru($lomba){
        $query="SELECT COUNT(noPendaftaran)noPendaftaran FROM $this->table WHERE jnlomba=?";
        $result=$this->db->query($query,[$lomba])->getResultArray();
        return $result;
    }

    // fungsi mencari nomor pendaftara berdasar nomor urut
    public function cekNomor($noUrut){
        return $this->where('noUrut',$noUrut)->findColumn('noPendaftaran');
    }
}