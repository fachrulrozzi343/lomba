<?php

namespace App\Controllers;

class Lomba extends BaseController
{
	protected $lomba;

	public function __construct()
	{
		$this->lomba = new \App\Models\PesertalombaModel();
	}

	// halman utama,menampilkan peserta lomba
	public function index(){
		$data=[
			'title'=>"lomba ekekul",
			'pstLukis'=>$this->lomba->pelomba('Lukis'),
			'pstPantomim'=>$this->lomba->pelomba('Pantomim'),
			'pstBulutangkis'=>$this->lomba->pelomba('Badminton'),
			'pstProgramming'=>$this->lomba->pelomba('Programming')

		];
		return view('lomba/index',$data);
	}

	//memanggil formulir pendaftaran
	public function ikut($lomba,$nuUrut=false){
		$noPendaftaran=$this->lomba->nomorBaru($lomba);
		$data = [
			'title' => 'Formulir Pendaftaran Lomba',
			'lomba' => $lomba,
			'noPendaftaran' => $noPendaftaran[0] ['noPendaftaran']
		];
		return view('lomba/formulir', $data);
	}

	//menambah data peserta
	public function daftar (){
		
		if ($this->lomba->tambahpeserta($_POST)==true){
			$this->session->setFlashdata('pesan','Pendaftaran Berhasil');
			return redirect()->to('/lomba');
		}else{
			$this->session->setFlashdata('pesan','Pendaftaran TIDAK Berhasil');
			return redirect()->to('/lomba');
		}
	}
	//fungsi detll peserta
	public function detail($lomba,$noUrut){
		$data=[
			'title'=>'peserta Lomba',
			'peserta'=>$this->lomba->pelomba($lomba,$noUrut)
		];
		return view('lomba/detail',$data);
	}
	//menghapus data peserta lomba
	public function mundur()
	{
		$data = [
			'noUrut' => $this->request->getVar('noUrut'),
			'noPendaftaran' => $this->request->getVar('noPendaftaran')
		];

		if($this->lomba->mundur($data) >0){
			$this->session->setFlashdata('pesan', 'Peserta telah mengundurkan diri');
		} else {
			$this->session->setFlashdata('pesan', 'Peserta tidak ditemukan');
		}
		return redirect()->to('/lomba');
	}
}